from django.apps import AppConfig


class ApakabarConfig(AppConfig):
    name = 'apakabar'
