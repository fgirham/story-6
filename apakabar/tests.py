from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index, tambahStatus
from .models import Status
from .forms import FormStatus
import chromedriver_binary
from chromedriver_binary.utils import get_chromedriver_path
from instapy_chromedriver import binary_path
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options


# Create your tests here.


class Story6UnitTest(TestCase):

    def test_story6_url_ada(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_story6_pakai_fungsi_index(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_template_yang_digunakan_benar(self):
        response = Client().get('')
        self.assertTemplateUsed(response, 'landingpage.html')

    def test_model_bisa_buat_status_baru(self):
        Status.objects.create(status='capek')
        self.assertEqual(Status.objects.all().count(), 1)

    def test_apakah_ada_tulisan_apa_kabar(self):
        response = Client().get('')
        self.assertContains(response, 'Apa Kabar')

    def test_apakah_ada_form(self):
        response = Client().get('')
        self.assertContains(response, '</form>')

    def test_apakah_ada_button_submit(self):
        response = Client().get('')
        self.assertContains(response, '</button>')

    def test_submit_pakai_fungsi_tambah(self):
        found = resolve('/submit/')
        self.assertEqual(found.func, tambahStatus)

    def test_teks_kosong_masuk_ke_models(self):
        form = FormStatus(data={'status': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['status'],
            ["This field is required."]
        )

    def test_representasi_string_model(self):
        status = Status(status = 'lelah')
        testString = 'lelah'
        self.assertEqual(str(status), testString)

    def test_status_post_sukses(self):
        response_post = Client().post('/submit/', {'status': 'tes'})
        self.assertEqual(response_post.status_code, 302)
    
        response= Client().get('')
        html_response = response.content.decode('utf8')
        self.assertIn('tes', html_response)
    
    def test_status_post_gagal(self):
        response_post = Client().post('/submit/', {'status': ''})
        self.assertEqual(response_post.status_code, 302)
    
        response= Client().get('')
        html_response = response.content.decode('utf8')
        self.assertNotIn('tes', html_response)


class Story6FunctionalTest(TestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(Story6FunctionalTest, self).setUp()


    def tearDown(self):
        self.selenium.quit()
        super(Story6FunctionalTest, self).tearDown()

    def test_input_status(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get('http://127.0.0.1:8000/')
        # find the form element
        form = selenium.find_element_by_id('id_status')

        button = selenium.find_element_by_id('add-btn')

        # # Fill the form with data
        form.send_keys('Coba Coba')

        # # submitting the form
        button.click()


