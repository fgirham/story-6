from django.contrib import admin
from django.urls import path
from .views import index, tambahStatus

urlpatterns = [
    path('', index, name='index'),
    path('submit/', tambahStatus, name='tambah')
]
