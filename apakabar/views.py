from django.shortcuts import render, redirect
from .models import Status
from .forms import FormStatus
from django.views.decorators.http import require_POST

# Create your views here.
def index(request):
    list_status = Status.objects.order_by('-waktu_input')
    form = FormStatus()
    context = {'list_status' : list_status , 'form' : form}
    return render(request, 'landingpage.html', context)

@require_POST
def tambahStatus(request):
    form = FormStatus(request.POST)
    
    if form.is_valid():
        status_baru = Status(status = request.POST['status'])
        status_baru.save()

    return redirect('index')